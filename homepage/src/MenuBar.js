
function MenuBar(props) {
    const menuItems = props.menuItems.map((item) =>
      <li key={item} className='valikonAlkio'><button>
          {item}
        </button>
      </li>
    )

    return(
        <div className="menuBar">
            <hr />
            <ul>
                {menuItems}
            </ul>
        </div>
    )
}

export default MenuBar;