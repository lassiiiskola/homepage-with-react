import './App.css';
import omaKuva from './oma_kuva.JPG'
import MenuBar from './MenuBar';

function App() {
  return (    
    <div>
      <div className='header'>
        <h1>LASSI IISKOLA</h1>
        <p>Työtä sekin tottelee!</p>
      </div>
      <div className='menuBar'>
        <MenuBar 
          menuItems={[
            "Etusivu",
            "Portfolio",
            "Historia",
            "Taidot"
          ]}/>
      </div>
    </div>
  );
}

export default App;
